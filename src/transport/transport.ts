import { Pokemon } from '@/model/model';

export interface IPokemonTransport {
  baseUrl: string;
  limit: number;
  fetchPokemonList(offset: number): Promise<Pokemon[]>;
  fetchPokemon(identifier: string | number): Promise<Pokemon>;
}

class PokemonTransport implements IPokemonTransport {
  baseUrl = 'https://pokeapi.co/api/v2/';
  limit = 20;

  public async fetchPokemonList(offset: number = 0): Promise<Pokemon[]> {
    try {
      const response = await fetch(
        `${this.baseUrl}pokemon?limit=${this.limit}&offset=${offset}`
      );

      const data = await response.json();
      const pokemonPromises = data.results.map(
        async (pokemon: { name: string; url: string }) => {
          const pokemonData = await fetch(pokemon.url);
          return pokemonData.json();
        }
      );

      return await Promise.all(pokemonPromises);
    } catch (error) {
      console.error('Ошибка запроса:', error);
      throw new Error('Ошибка запроса');
    }
  }
  public async fetchPokemon(id: string | number): Promise<Pokemon> {
    try {
      const response = await fetch(`${this.baseUrl}/pokemon/${id}/`);
      if (!response.ok) {
        throw new Error(`Ошибка запроса: ${response.statusText}`);
      }
      return await response.json();
    } catch (error) {
      console.error('Ошибка запроса:', error);
      throw new Error('Ошибка запроса');
    }
  }
}

export const pokemonTransport = new PokemonTransport();
