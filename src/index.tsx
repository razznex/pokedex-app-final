import '../common/styles/reset/reset.scss';
import '../common/styles/fonts/fonts.scss';
import styles from '@/view/Main.module.scss';
import React from 'react';
import ReactDOM from 'react-dom/client';
import View from '@/view/view';
import { BrowserRouter } from 'react-router-dom';

const root = ReactDOM.createRoot(document.getElementById('root'));
document.getElementById('root').classList.add(styles.root);

root.render(
  <>
    <BrowserRouter>
      <View />
    </BrowserRouter>
  </>
);
