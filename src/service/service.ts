import { catchedLocalPokemons, Pokemon } from '@/model/model';
import { pokemonTransport } from '@/transport/transport';
import { formatDateTime } from '../../common/utils/dateTimeFormat';
import { pokemonStore } from '@/store/store';
import { runInAction } from 'mobx';

interface IPokemonService {
  offset: number;
  catchPokemon(pokemon: Pokemon): void;
  loadPokemons(): Promise<void>;
  loadMorePokemons(): Promise<void>;
  getPokemon(id: string | number): Promise<void>;
}

class PokemonService implements IPokemonService {
  offset = 0;

  public catchPokemon(pokemon: Pokemon): void {
    const date = new Date().toISOString();
    const formattedDate = formatDateTime(date);
    runInAction(() => {
      pokemon.caught = true;
      pokemon.dateCaught = formattedDate;
      pokemonStore.catchPokemon(pokemon);

      const currentCatchedPokemons: catchedLocalPokemons[] = JSON.parse(
        localStorage.getItem('catchedPokemons') || '[]'
      );

      currentCatchedPokemons.push({
        id: pokemon.id,
        dateCaught: pokemon.dateCaught,
        caught: pokemon.caught,
      });

      localStorage.setItem(
        'catchedPokemons',
        JSON.stringify(currentCatchedPokemons)
      );
    });
  }

  public async loadPokemons(): Promise<void> {
    pokemonStore.setIsLoading(true);
    pokemonStore.setError(null);
    try {
      const pokemons = await pokemonTransport.fetchPokemonList(this.offset);
      pokemonStore.setPokemons(pokemons);
      this.offset += 20;
    } catch (error: any) {
      pokemonStore.setError(error.message);
    } finally {
      pokemonStore.setIsLoading(false);
    }
  }

  public async loadMorePokemons(): Promise<void> {
    pokemonStore.setIsFetching(true);
    pokemonStore.setError(null);
    try {
      const pokemons = await pokemonTransport.fetchPokemonList(this.offset);
      pokemonStore.setPokemons(pokemons);
      this.offset += 20;
    } catch (error: any) {
      pokemonStore.setError(error.message);
    } finally {
      pokemonStore.setIsFetching(false);
    }
  }

  public async getPokemon(id: string | number): Promise<void> {
    pokemonStore.setIsLoading(true);
    try {
      const pokemon = await pokemonTransport.fetchPokemon(id);
      pokemonStore.getOnePokemon(pokemon);
    } catch (error) {
      pokemonStore.setError(error.message);
      throw error;
    } finally {
      pokemonStore.setIsLoading(false);
    }
  }

  public async loadCatchedPokemons(): Promise<void> {
    try {
      const catchedPokemonsById = JSON.parse(
        localStorage.getItem('catchedPokemons') || '[]'
      );
      const pokemons: Pokemon[] = await Promise.all(
        catchedPokemonsById.map(async (p: catchedLocalPokemons) => {
          const pokemon = await pokemonTransport.fetchPokemon(p.id);
          return { ...pokemon, caught: true, dateCaught: p.dateCaught };
        })
      );
      pokemons.sort((firstPokemon, secondPokemon) =>
        firstPokemon.id > secondPokemon.id ? 1 : -1
      );
      runInAction(() => {
        pokemonStore.catchedPokemons = [];
        pokemons.forEach((pokemon) => pokemonStore.catchPokemon(pokemon));
      });
    } catch (error) {
      runInAction(() => {
        pokemonStore.setError(error.message);
      });
    }
  }
}

export const pokemonService = new PokemonService();
