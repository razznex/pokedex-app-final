import React from 'react';
import styles from './PageNotFound.module.scss';
import { Link } from 'react-router-dom';
import luxio from '../../../common/icons/404.png'

const PageNotFound = () => {
  return (
    <div className={styles.notfound}>
      <p className={styles.notfound__title}>Страница не найдена</p>
      <div className={styles.notfound__container}>
        <img
          src={luxio}
          alt='Luxio'
          className={styles.notfound__image}
        />
        <div className={styles.notfound__textContainer}>
          <p className={styles.notfound__id}>№404</p>
          <p className={styles.notfound__text}>Luxio</p>
        </div>
      </div>
      <Link to='/' className={styles.notfound__back}>
        На главную
      </Link>
    </div>
  );
};

export default PageNotFound;
