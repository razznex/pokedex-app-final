import React, { useEffect } from 'react';
import styles from './Pokedex.module.scss';
import { pokemonService } from '@/service/service';
import { useNavigate, useParams } from 'react-router-dom';
import Preloader from '@/view/Preloader/Preloader';
import PokemonInfo from '@/view/Pokedex/PokemonInfo/PokemonInfo';
import { POKEMON_NUMBER } from '../../../common/constants/constants';
import { pokemonStore } from '@/store/store';
import { observer } from 'mobx-react-lite';

const Pokedex = observer(() => {
  const { isLoading, onePokemon } = pokemonStore;
  let { id } = useParams();
  const navigate = useNavigate();
  let idPokemon = Number(id);
  const isValidId = (idPokemon: number) => {
    if (!Number.isNaN(idPokemon)) {
      if (idPokemon > 0 && idPokemon <= POKEMON_NUMBER) return true;
    } else {
      return false;
    }
  };

  useEffect(() => {
    if (!isValidId(idPokemon)) {
      navigate('/not-found');
    } else {
      pokemonService.getPokemon(idPokemon);
    }
  }, []);

  return (
    <>
      {isLoading ? (
        <Preloader />
      ) : (
        <>
          <div className={styles.pokedex}>
            <div className={styles.pokedex__top}>
              <div className={styles.pokedex__screen}>
                <PokemonInfo />
              </div>
            </div>
            <div className={styles.pokedex__bottom}>
              <div className={styles.pokedex__control}>
                <button
                  className={styles.pokedex__back}
                  onClick={() => navigate(-1)}
                >
                  Назад
                </button>
                <div
                  className={`${styles.pokedex__button} ${styles.pokedex__button_small}`}
                ></div>
              </div>
            </div>
          </div>
        </>
      )}
    </>
  );
});

export default Pokedex;
