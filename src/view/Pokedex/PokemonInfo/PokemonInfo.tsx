import React from 'react';
import { typeIcons } from '../../../../common/icons/types/PokemonTypes';
import { Pokemon, PokemonType } from '@/model/model';
import PokemonStatTable from '@/view/PokemonStatTable/PokemonStatTable';
import styles from './PokemonInfo.module.scss';
import { pokemonStore } from '@/store/store';
import { observer } from 'mobx-react-lite';

const PokemonInfo = observer(() => {
  const { onePokemon } = pokemonStore;
  const getDateCaught = (id: number): string | undefined => {
    const pokemon = pokemonStore.pokemons.find(
      (pokemon: Pokemon) => pokemon.id === id
    );
    return pokemon ? pokemon.dateCaught : undefined;
  };

  return (
    <div className={styles.pokemonInfo}>
      <h2 className={styles.pokemonInfo__title}>
        {onePokemon?.name}{' '}
        <span className={styles.pokemonInfo__id}>№{onePokemon?.id}</span>
      </h2>
      <div className={styles.pokemonInfo__types}>
        {onePokemon?.types.map((typeObj) => {
          const typeName = typeObj.type.name;
          if (typeName in typeIcons) {
            return (
              <div
                key={typeObj.type.name}
                className={styles.pokemonInfo__typesContainer}
              >
                <img
                  src={typeIcons[typeName as PokemonType]}
                  alt={typeName}
                  className={styles.pokemonInfo__type}
                />
                <div className={styles.pokemonInfo__typeTooltip}>
                  {typeName}
                </div>
              </div>
            );
          }
          return null;
        })}
      </div>
      <img
        src={`https://www.pokemon.com/static-assets/content-assets/cms2/img/pokedex/full/${onePokemon?.id < 10 ? `00${onePokemon?.id}` : onePokemon?.id < 100 ? `0${onePokemon?.id}` : onePokemon?.id}.png`}
        alt={onePokemon?.name}
        className={styles.pokemonInfo__image}
      />
      {getDateCaught(onePokemon?.id) && (
        <p className={styles.pokemonInfo__caught}>
          Пойман: {getDateCaught(onePokemon?.id)}
        </p>
      )}
      <div className={styles.pokemonInfo__container}>
        <p className={styles.pokemonInfo__abilitiesName}>Способности:</p>
        <ul className={styles.pokemonInfo__abilities}>
          {onePokemon?.abilities.map((abilityObj) => (
            <li
              key={abilityObj.ability.name}
              className={styles.pokemonInfo__abilitiy}
            >
              {abilityObj.ability.name}
            </li>
          ))}
        </ul>
      </div>
      <div className={styles.pokemonInfo__table}>
        <PokemonStatTable pokemon={onePokemon} />
      </div>
    </div>
  );
});

export default PokemonInfo;
