import styles from './Preloader.module.scss';

import React from 'react';

const Preloader = () => {
  return <div className={styles.pokeball}></div>;
};

export default Preloader;
