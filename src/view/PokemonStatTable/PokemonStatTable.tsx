import React from 'react';
import { Pokemon } from '@/model/model';
import styles from './PokemonStatTable.module.scss';

interface PokemonStatTableProps {
  pokemon: Pokemon;
}

const PokemonStatTable: React.FC<PokemonStatTableProps> = ({ pokemon }) => (
  <div className={styles.stats}>
    <table className={styles.stats__table}>
      <tbody>
        {pokemon?.stats.map((statObj) => (
          <tr key={statObj.stat.name}>
            <td className={styles.stats__text} key={statObj.stat.name}>
              {statObj.stat.name}
            </td>
            <td className={`${styles.stats__text} ${styles.stats__text_small}`}>
              {' '}
              {statObj.base_stat}
            </td>
            <td className={styles.stats__progressBarContainer}>
              <div
                className={styles.stats__progressBar}
                style={{ width: `${statObj.base_stat * 0.625}%` }}
              ></div>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  </div>
);

export default PokemonStatTable;
