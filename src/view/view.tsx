import React from 'react';
import Header from '@/view/Header/Header';
import PokemonMain from '@/view/PokemonList/PokemonMain';
import styles from './Main.module.scss';
import { Route, Routes } from 'react-router-dom';
import Pokedex from '@/view/Pokedex/Pokedex';
import PageNotFound from '@/view/PageNotFound/PageNotFound';

const View = () => {
  return (
    <>
      <Header />
      <main className={styles.main}>
        <Routes>
          <Route path='/' element={<PokemonMain />} />
          <Route path='/catched' element={<PokemonMain />} />
          <Route path='/:id' element={<Pokedex />} />
          <Route path='*' element={<PageNotFound />} />
          <Route path='/not-found' element={<PageNotFound />} />
        </Routes>
      </main>
    </>
  );
};

export default View;
