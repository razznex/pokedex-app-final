import React from 'react';
import styles from '@/view/PokemonList/PokemonMain.module.scss';
import professor from '../../../../common/icons/professor.png';

const NoPokemon = () => {
  return (
    <div className={styles.pokemon__container}>
      <p className={styles.pokemon__text}>
        Ты&nbsp;не&nbsp;поймал ещё ни&nbsp;одного покемона
      </p>
      <img
        src={professor}
        className={styles.pokemon__img}
        alt='Профессор Оук'
      />
    </div>
  );
};

export default NoPokemon;
