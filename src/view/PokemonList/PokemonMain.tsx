import React, { useEffect, useState } from 'react';
import PokemonCard from '@/view/PokemonCard/PokemonCard';
import styles from './PokemonMain.module.scss';
import { Pokemon } from '@/model/model';
import { pokemonService } from '@/service/service';
import Preloader from '@/view/Preloader/Preloader';
import { useLocation, useNavigate } from 'react-router-dom';
import NoPokemon from '@/view/PokemonList/NoPokemon/NoPokemon';
import { POKEMON_NUMBER } from '../../../common/constants/constants';
import { pokemonStore } from '@/store/store';
import { observer } from 'mobx-react-lite';
import ButtonScrollUp from '@/Components/ButtonScrollUp/ButtonScrollUp';
import PokemonList from '@/Components/PokemonList/PokemonList';
import CatchedPokemonList from '@/Components/PokemonList/CatchedPokemonList';

const PokemonMain = observer(() => {
  const { pokemons, isLoading } = pokemonStore;
  const location = useLocation();

  useEffect(() => {
    if (pokemons.length === 0) {
      pokemonService.loadPokemons();
    }
    pokemonService.loadCatchedPokemons();
  }, [pokemons]);

  return (
    <>
      {isLoading ? (
        <Preloader />
      ) : (
        <>
          {location.pathname === '/' ? <PokemonList /> : <CatchedPokemonList />}
        </>
      )}
    </>
  );
});

export default PokemonMain;
