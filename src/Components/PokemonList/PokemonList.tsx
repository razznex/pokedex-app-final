import React, { useEffect } from 'react';
import styles from '@/view/PokemonList/PokemonMain.module.scss';
import PokemonCard from '@/view/PokemonCard/PokemonCard';
import Preloader from '@/view/Preloader/Preloader';
import ButtonScrollUp from '@/Components/ButtonScrollUp/ButtonScrollUp';
import { Pokemon } from '@/model/model';
import { pokemonService } from '@/service/service';
import { pokemonStore } from '@/store/store';
import { POKEMON_NUMBER } from '../../../common/constants/constants';

const PokemonList = () => {
  const { pokemons, catchedPokemons, isFetching } = pokemonStore;
  const onCatch = (pokemon: Pokemon) => {
    pokemonService.catchPokemon(pokemon);
  };

  useEffect(() => {
    if (isFetching) {
      pokemonService.loadMorePokemons();
    }
  }, [isFetching]);

  const handleScroll = async (e: Event) => {
    const target = e.target as Document;
    if (
      target.documentElement.scrollHeight -
        (target.documentElement.scrollTop + window.innerHeight) <
        100 &&
      pokemons.length < POKEMON_NUMBER
    ) {
      if (!isFetching) {
        pokemonStore.setIsFetching(true);
      }
    }
  };

  useEffect(() => {
    document.addEventListener('scroll', handleScroll);
    return () => {
      document.removeEventListener('scroll', handleScroll);
    };
  }, [isFetching]);

  const mergedPokemons = pokemons.map((pokemon) => {
    const mergePokemons = catchedPokemons.find(
      (catched) => catched.id === pokemon.id
    );
    return mergePokemons ? { ...pokemon, ...mergePokemons } : pokemon;
  });

  return (
    <section className={styles.pokemon}>
      <div className={styles.pokemon__cards}>
        {mergedPokemons.map((pokemon) => (
          <PokemonCard key={pokemon.name} pokemon={pokemon} onCatch={onCatch} />
        ))}
      </div>
      {isFetching ? <Preloader /> : ''}
      <ButtonScrollUp />
    </section>
  );
};

export default PokemonList;
