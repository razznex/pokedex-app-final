import React from 'react';
import styles from '@/view/PokemonList/PokemonMain.module.scss';
import PokemonCard from '@/view/PokemonCard/PokemonCard';
import ButtonScrollUp from '@/Components/ButtonScrollUp/ButtonScrollUp';
import NoPokemon from '@/view/PokemonList/NoPokemon/NoPokemon';
import { pokemonStore } from '@/store/store';

const CatchedPokemonList = () => {
  const { catchedPokemons } = pokemonStore;

  return (
    <div>
      {catchedPokemons.length !== 0 ? (
        <section className={styles.pokemon}>
          <div className={styles.pokemon__cards}>
            {catchedPokemons.map((pokemon) => (
              <PokemonCard key={pokemon.id} pokemon={pokemon} />
            ))}
          </div>
          <ButtonScrollUp />
        </section>
      ) : (
        <NoPokemon />
      )}
    </div>
  );
};

export default CatchedPokemonList;
