import React, { useState } from 'react';
import styles from './ButtonScrollUp.module.scss';

const ButtonScrollUp = () => {
  const [scrollPosition, setScrollPosition] = useState<number>(0);

  window.addEventListener('scroll', () => {
    setScrollPosition(window.scrollY);
  });
  return (
    <div>
      <button
        className={
          scrollPosition < 200
            ? styles.button__scroll_hidden
            : styles.button__scroll
        }
        onClick={() => {
          window.scrollTo({ top: 0, behavior: 'smooth' });
        }}
      >
        &#8593;
      </button>
    </div>
  );
};

export default ButtonScrollUp;
