import { makeAutoObservable } from 'mobx';
import { Pokemon } from '@/model/model';

class PokemonStore {
  pokemons: Pokemon[] = [];
  onePokemon: Pokemon = null;
  catchedPokemons: Pokemon[] = [];
  isLoading: boolean = true;
  isFetching: boolean = false;
  error: string | null = null;

  constructor() {
    makeAutoObservable(this);
  }

  public setPokemons(pokemons: Pokemon[]) {
    this.pokemons = [...this.pokemons, ...pokemons];
  }

  public setIsLoading(isLoading: boolean) {
    this.isLoading = isLoading;
  }

  public setIsFetching(isFetching: boolean) {
    this.isFetching = isFetching;
  }

  public setError(error: string | null) {
    this.error = error;
  }

  public catchPokemon(pokemon: Pokemon) {
    this.catchedPokemons.push(pokemon);
  }

  public getOnePokemon(pokemon: Pokemon) {
    this.onePokemon = pokemon;
  }
}

export const pokemonStore = new PokemonStore();
